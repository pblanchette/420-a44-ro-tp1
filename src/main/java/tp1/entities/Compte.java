package tp1.entities;

import lombok.Data;

@Data
public class Compte {

  private String nom;
  private double montant;
  private int limiteOperation;
  private double limiteMontant;
  private int nombreOperation;
  private double montantJour;

  public Compte(String nom, double montant, int limiteOperation, double limiteMontant) {
    super();
    this.nom = nom;
    this.montant = montant;
    this.limiteOperation = limiteOperation;
    this.limiteMontant = limiteMontant;
    this.nombreOperation = 0;
    this.montantJour = 0;
  }

}
