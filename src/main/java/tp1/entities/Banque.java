package tp1.entities;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;

@Data
public class Banque {

  private Map<String, Compte> comptes;

  public Banque() {
    super();
    this.comptes = new HashMap<String, Compte>();
  }

}
