package tp1.services;

import tp1.entities.Compte;

public class CompteService {

  private static CompteService compteService;

  private CompteService() {
    super();
  }

  public static CompteService obtenirCompteService() {
    if (compteService == null) {
      return new CompteService();
    }
    return compteService;
  }


  /*
   * Ajouter les validations suivante:
   * 
   * Le nom ne doit pas être vide ou dépasser 40 charactères
   * 
   * Le montant ne doit pas être négatif
   * 
   * La limite du nombre d'opérations ne doit pas être négatif ni dépasser 20
   * 
   * La limite de retrait par jour ne doit pas être négatif ni être supérieur à 100
   * 
   * Une validation transgressée doit lancer une exeption avec un message qui explique la nature du problème
   */
  public Compte creerCompte(String nom, double montant, int limiteOperation, double limiteMontant) {
    return new Compte(nom, montant, limiteOperation, limiteMontant);
  }

  /*
   * Ajouter les validations suivante:
   * 
   * Le nombre d'opération ne doit pas dépasser la limite
   * 
   * Le montant ne doit pas être négatif
   * 
   * Un montant invalide doit lancer une exeption avec un message qui explique la nature du problème
   */
  public double depot(Compte compte, double montant) {
    compte.setNombreOperation(compte.getNombreOperation() + 1);
    compte.setMontant(compte.getMontant() + montant);
    return compte.getMontant();
  }

  /*
   * Ajouter les validations suivante:
   * 
   * Le nombre d'opération ne doit pas dépasser la limite
   * 
   * Le montant par jour ne doit pas dépasser la limite
   * 
   * Le montant ne doit pas être négatif
   * 
   * Le compte doit avoir suffisament d'argent
   * 
   * Un montant invalide doit lancer une exeption avec un message qui explique la nature du problème
   */
  public double retrait(Compte compte, double montant) {
    compte.setNombreOperation(compte.getNombreOperation() + 1);
    compte.setMontantJour(compte.getMontantJour() + montant);
    compte.setMontant(compte.getMontant() - montant);
    return compte.getMontant();
  }


  /*
   * Ajouter les validations suivante:
   * 
   * La limite du nombre d'opérations ne doit pas être négatif ni dépasser 20
   * 
   * La limite de retrait par jour doit être supérieur à 100
   * 
   * Un montant invalide doit lancer une exeption avec un message qui explique la nature du problème
   */
  public void modifierCompte(Compte compte, int limiteOperation, double limiteMontant) {
    compte.setLimiteOperation(limiteOperation);
    compte.setLimiteMontant(limiteMontant);
  }

}
