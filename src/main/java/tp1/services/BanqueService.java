package tp1.services;

import java.util.Collection;
import tp1.entities.Banque;
import tp1.entities.Compte;

public class BanqueService {

  private static BanqueService banqueService;

  private BanqueService() {
    super();
  }

  public static BanqueService obtenirBanqueService() {
    if (banqueService == null) {
      return new BanqueService();
    }
    return banqueService;
  }

  public Collection<Compte> obtenirComptes(Banque banque) {
    return banque.getComptes().values();
  }

  /*
   * Ajouter une exception lorsque le compte n'existe pas.
   */
  public Compte trouverCompte(Banque banque, String nom) {
    return banque.getComptes().get(nom);
  }

  /*
   * Ajouter une exception lorsque le nom du compte n'est pas unique.
   */
  public Compte ouvrirCompte(Banque banque, String nom, double montant, int limiteOperation,
      double limiteMontant) {
    Compte compte = CompteService.obtenirCompteService().creerCompte(nom, montant, limiteOperation,
        limiteMontant);
    banque.getComptes().put(nom, compte);
    return compte;
  }

  /*
   * Ajouter une exception lorsque le compte n'existe pas.
   */
  public void fermerCompte(Banque banque, String nom) {
    banque.getComptes().remove(nom);
  }

}
