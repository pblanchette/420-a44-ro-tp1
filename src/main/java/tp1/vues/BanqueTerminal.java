package tp1.vues;

import java.util.Collection;
import java.util.Scanner;
import tp1.entities.Banque;
import tp1.entities.Compte;
import tp1.services.BanqueService;
import tp1.services.CompteService;

public class BanqueTerminal {

  private static BanqueTerminal banqueTerminal;

  private Scanner sc;

  private Banque banque;

  private BanqueService banqueService;

  private CompteService compteService;

  private BanqueTerminal() {
    super();
    this.sc = new Scanner(System.in);
    this.banque = new Banque();
    this.banqueService = BanqueService.obtenirBanqueService();
    this.compteService = CompteService.obtenirCompteService();
  }

  public static BanqueTerminal obtenirBanqueTerminal() {
    if (banqueTerminal == null) {
      return new BanqueTerminal();
    }
    return banqueTerminal;
  }

  public void menuPrincipal() {
    String choix;
    boolean valide;
    do {
      System.out.println("Menu principal:");
      System.out.println("l: Liste des comptes");
      System.out.println("n: Ouvrir un compte");
      System.out.println("c: Consulter un compte");
      System.out.println("f: Fermer un compte");
      System.out.println("q: Quitter");
      do {
        valide = true;
        System.out.print("Choix:");
        choix = sc.next();
        switch (choix) {
          case "l":
            listeComptes();
            break;
          case "n":
            ouvrirComptes();
            break;
          case "c":
            consulterCompte();
            break;
          case "f":
            fermerCompte();
            break;
          case "q":
            break;
          default:
            System.out.println("Le menu ne contient pas d'option: " + choix);
            valide = false;
        }
      } while (!valide);
    } while (!choix.equals("q"));

  }

  public void listeComptes() {
    Collection<Compte> comptes = banqueService.obtenirComptes(banque);
    if (comptes.isEmpty()) {
      System.out.println("Il n'y a pas de compte");
    } else {
      comptes.forEach(compte -> System.out.println(compte.getNom()));
    }
  }

  /*
   * Ajouter la gestion des exeptions
   * 
   * En cas d'exception, afficher le message d'erreur et revener au menu principal
   */
  public void ouvrirComptes() {
    String nom;
    double montant;
    int limiteOperation;
    double limiteMontant;
    System.out.print("Entrer le nom du compte: ");
    nom = sc.next();
    System.out.print("Entrer le montant initiale: ");
    montant = sc.nextDouble();
    System.out.print("Entrer la limite d'opération par jour: ");
    limiteOperation = sc.nextInt();
    System.out.print("Entrer la limite de montant d'argent retiré par jour: ");
    limiteMontant = sc.nextDouble();
    banqueService.ouvrirCompte(banque, nom, montant, limiteOperation, limiteMontant);
    System.out.println("Le compte " + nom + " a été crée");
  }

  /*
   * Ajouter la gestion des exeptions
   * 
   * En cas d'exception, afficher le message d'erreur et revener au menu principal
   */
  public void consulterCompte() {
    String choix;
    Compte compte;
    System.out.println("Entrer le nom du compte à consulter:");
    System.out.print("Choix:");
    choix = sc.next();
    compte = banqueService.trouverCompte(banque, choix);
    menuCompte(compte);
  }

  /*
   * Ajouter la gestion des exeptions
   * 
   * En cas d'exception, afficher le message d'erreur et revener au menu principal
   */
  public void fermerCompte() {
    String choix;
    System.out.println("Entrer le nom du compte à consulter:");
    System.out.print("Choix:");
    choix = sc.next();
    banqueService.fermerCompte(banque, choix);
  }

  public void menuCompte(Compte compte) {
    String choix;
    boolean valide;
    do {
      System.out.println("Menu du compte:");
      System.out.println("i: Information");
      System.out.println("d: Dépot");
      System.out.println("r: Retrait");
      System.out.println("m: Modifier");
      System.out.println("q: Quitter");
      do {
        valide = true;
        System.out.print("Choix:");
        choix = sc.next();
        switch (choix) {
          case "i":
            informationCompte(compte);
            break;
          case "d":
            depotCompte(compte);
            break;
          case "r":
            retraitCompte(compte);
            break;
          case "m":
            modifierCompte(compte);
            break;
          case "q":
            break;
          default:
            System.out.println("Le menu ne contient pas d'option: " + choix);
            valide = false;
        }
      } while (!valide);
    } while (!choix.equals("q"));
  }

  private void informationCompte(Compte compte) {
    System.out.println(compte);
  }

  /*
   * Ajouter la gestion des exeptions
   * 
   * En cas d'exception, afficher le message d'erreur et revener au menu compte
   */
  private void depotCompte(Compte compte) {
    double montant;
    System.out.print("Entrer le montant a déposer: ");
    montant = sc.nextDouble();
    compteService.depot(compte, montant);
  }

  /*
   * Ajouter la gestion des exeptions
   * 
   * En cas d'exception, afficher le message d'erreur et revener au menu compte
   */
  private void retraitCompte(Compte compte) {
    double montant;
    System.out.print("Entrer le montant a retirer: ");
    montant = sc.nextDouble();
    compteService.retrait(compte, montant);
  }

  /*
   * Ajouter la gestion des exeptions
   * 
   * En cas d'exception, afficher le message d'erreur et revener au menu compte
   */
  private void modifierCompte(Compte compte) {
    int limiteOperation;
    double limtieMontant;
    System.out.print("Entrer la nouvelle limite d'opération par jour: ");
    limiteOperation = sc.nextInt();
    System.out.print("Entrer la nouvelle limite de retrait par jour: ");
    limtieMontant = sc.nextDouble();
    compteService.modifierCompte(compte, limiteOperation, limtieMontant);
  }

}
