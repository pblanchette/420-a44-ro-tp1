package tp1;

import tp1.vues.BanqueTerminal;

public class Application {

  public static void main(String[] args) {
    BanqueTerminal banqueTerminal = BanqueTerminal.obtenirBanqueTerminal();
    banqueTerminal.menuPrincipal();
  }

}

